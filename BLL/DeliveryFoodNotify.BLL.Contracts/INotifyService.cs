namespace DeliveryFoodNotify.BLL.Contracts;

public interface INotifyService
{
    public Task SendNotify(string message);
}