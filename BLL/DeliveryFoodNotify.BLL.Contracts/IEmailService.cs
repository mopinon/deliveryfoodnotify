namespace DeliveryFoodNotify.BLL.Contracts;

public interface IEmailService
{
    public Task<SendEmailResult> SendEmailAsync(string email, string subject, string message);
}