namespace DeliveryFoodNotify.BLL.Contracts;

public enum SendEmailResult
{
    Ok,
    InvalidEmail
}