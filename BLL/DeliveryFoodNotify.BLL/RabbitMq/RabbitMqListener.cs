using System.Text;
using DeliveryFoodNotify.BLL.Contracts;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace DeliveryFoodNotify.BLL.RabbitMq;

public class RabbitMqListener : BackgroundService
{
    private IConnection _connection;
    private IModel _channel;
    private readonly INotifyService _notifyService;

    public RabbitMqListener(INotifyService notifyService)
    {
        _notifyService = notifyService;
        var factory = new ConnectionFactory { HostName = "37.230.114.216" };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.QueueDeclare(queue: "Notify", durable: false, exclusive: false, autoDelete: false, arguments: null);
    }
    
    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        stoppingToken.ThrowIfCancellationRequested();

        var consumer = new EventingBasicConsumer(_channel);
        consumer.Received += (ch, ea) =>
        {
            var content = Encoding.UTF8.GetString(ea.Body.ToArray());
            var task = _notifyService.SendNotify(content);
            task.Start();
            _channel.BasicAck(ea.DeliveryTag, false);
        };

        _channel.BasicConsume("Notify", false, consumer);

        return Task.CompletedTask;
    }
	
    public override void Dispose()
    {
        _channel.Close();
        _connection.Close();
        base.Dispose();
    }
}