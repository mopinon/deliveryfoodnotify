using DeliveryFoodNotify.BLL.Contracts;
using MailKit.Net.Smtp;
using MimeKit;

namespace DeliveryFoodNotify.BLL;

public class EmailService : IEmailService
{
    private static string Email => Environment.GetEnvironmentVariable("EMAIL");
    private static string Password => Environment.GetEnvironmentVariable("EMAIL_PASSWORD");

    public async Task<SendEmailResult> SendEmailAsync(string email, string subject, string message)
    {
        var emailMessage = new MimeMessage();

        emailMessage.From.Add(new MailboxAddress("Systema", Email));
        emailMessage.To.Add(new MailboxAddress("", email));
        emailMessage.Subject = subject;
        emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
        {
            Text = message
        };
        using var client = new SmtpClient();
        await client.ConnectAsync("smtp.yandex.ru", 25, false);
        await client.AuthenticateAsync(Email, Password);
        await client.SendAsync(emailMessage);
        await client.DisconnectAsync(true);
        return SendEmailResult.Ok;
    }
}