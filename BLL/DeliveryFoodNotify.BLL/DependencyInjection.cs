using DeliveryFoodNotify.BLL.Contracts;
using DeliveryFoodNotify.BLL.RabbitMq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeliveryFoodNotify.BLL;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHostedService<RabbitMqListener>();
        services.AddTransient<INotifyService, NotifyService>();
        services.AddTransient<IEmailService, EmailService>();
        
        return services;
    }
}