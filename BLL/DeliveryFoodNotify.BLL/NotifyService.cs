using DeliveryFoodNotify.BLL.Contracts;
using DeliveryFoodNotify.BLL.Dto;
using Newtonsoft.Json;

namespace DeliveryFoodNotify.BLL;

public class NotifyService : INotifyService
{
    private readonly IEmailService _emailService;

    public NotifyService(IEmailService emailService)
    {
        _emailService = emailService;
    }

    public async Task SendNotify(string message)
    {
        var notify = JsonConvert.DeserializeObject<Notify>(message);
        await _emailService.SendEmailAsync(notify.Email, notify.Subject, notify.Message);
    }
}