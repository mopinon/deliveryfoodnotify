﻿namespace DeliveryFoodNotify.BLL.Dto;

public class Notify
{
    public string Email { get; set; }
    public string Subject { get; set; }
    public string Message { get; set; }
}