docker build -t mopin/delivery-food-notify -f ./Web/DeliveryFoodNotify.Web/Dockerfile .
docker push  mopin/delivery-food-notify
ssh otus <<'ENDSSH'
    cd /home/delivery-food-notify/
    docker pull mopin/delivery-food-notify
    docker-compose up -d
ENDSSH